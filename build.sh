#!/bin/bash

cd $(dirname $0)

[[ $REPLICATED_API_TOKEN == "" || $REPLICATED_APP == "" ]] && echo 'Please set env vars: REPLICATED_API_TOKEN and REPLICATED_APP' >&2 && exit 1


export jcVersion=$(yq e '.appVersion' helm/jama/Chart.yaml)
echo $jcVersion

if [[ 1 == 2 ]]; then
## build the jama docker image
(cd app
docker image build -t registry.replicated.com/jrep/jama-testapp:latest -t registry.replicated.com/jrep/jama-testapp:$jcVersion .
echo "jama-app:$jcVersion"
#sudo docker image push jama-app:$jcVersion
docker login registry.replicated.com -u $REPLICATED_API_TOKEN -p $REPLICATED_API_TOKEN
docker push -a registry.replicated.com/jrep/jama-testapp
docker image prune -af
)
fi

## get the dependencies and create the Jama helm pkg
(cd helm
rm -rf jama/charts/*
helm dependency update jama
helm package ./jama --version $jcVersion -d ../kots/manifests/
)

cat <<EOF > kots/manifests/jama.yaml
apiVersion: kots.io/v1beta1
kind: HelmChart
metadata:
  name: jama
spec:
  namespace: "{{repl ChannelName | lower }}"
  chart:
    name: jama
    chartVersion: $jcVersion
  values:
    adminUser: 'admin'
    adminPassword: 'admin'
EOF

# create the KoTS pkg, deploy and prmote on UNSTABLE branch
(cd kots
replicated release create --auto -y --promote Beta)

