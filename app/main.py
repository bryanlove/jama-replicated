import sys
import os
import stomp
import time
import threading
import logging

user = os.getenv("ACTIVEMQ_USER") or "admin"
password = os.getenv("ACTIVEMQ_PASSWORD") or "password"
host = os.getenv("ACTIVEMQ_HOST") or "jama-activemq"
#host = os.getenv("ACTIVEMQ_HOST") or "localhost"
destination = os.getenv("ACTIVEMQ_QUEUE") or ["blove"]
destination = "/queue/" + destination
logging.basicConfig(format="%(asctime)s: %(message)s", level=logging.INFO, datefmt="%H:%M:%S")


class MyListener(stomp.ConnectionListener):
    def on_error(self, message):
        logging.info("Listener    : ERROR")
        print("%s" % message)

    def on_message(self, message):
        logging.info("Main    : received msg")
        print("%s" % message)


def publish(self):
    # conn = stomp.Connection([(host, 32626)]) #61613
    conn = stomp.Connection([(host, 61613)])
    conn.connect(login=user,passcode=password)
    messages = 1000
    data = "Hello World from Python"

    for i in range(0, messages):
        conn.send(destination, data, persistent='false')
        time.sleep(1)

    conn.send(destination, "SHUTDOWN", persistent='false')
    conn.disconnect()


def consume(self):
    # conn = stomp.Connection([(host, 32626)])
    conn = stomp.Connection([(host, 61613)])
    listener = MyListener()
    conn.set_listener('myListener', listener)
    conn.connect(login=user, passcode=password)
    conn.subscribe(destination, "testConsumer")


if __name__ == "__main__":

    logging.info("Main    : starting publisher")
    x = threading.Thread(target=publish, args=(1,))
    x.start()

    logging.info("Main    : starting consumer")
    y = threading.Thread(target=consume, args=(1,))
    y.start()

    x.join()
    y.join()





